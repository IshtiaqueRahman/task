import 'package:flutter/material.dart';
import 'package:task/trip.dart';
import 'consts.dart';
import 'custom_appbar_widget.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home : DoctorList()
  )
  );
}

class DoctorList extends StatefulWidget {
  @override
  _DoctorListState createState() => _DoctorListState();
}

class _DoctorListState extends State<DoctorList> {
  @override
  Widget build(BuildContext context) {
    final List<Trip> tripsList = [
      Trip("New York", DateTime.now(), DateTime.now(), 200.00, "car"),
      Trip("Boston", DateTime.now(), DateTime.now(), 450.00, "plane"),
      Trip("Washington D.C.", DateTime.now(), DateTime.now(), 900.00, "bus"),
      Trip("Austin", DateTime.now(), DateTime.now(), 170.00, "car"),
      Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
      Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
      Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
      Trip("Scranton", DateTime.now(), DateTime.now(), 180.00, "car"),
    ];
    return new Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: AppBar(
           backgroundColor: Colors.white,
           title: Center(child: Text("Hello Appbar",style: TextStyle(
              color: Colors.black
           )),
          ),
          leading: GestureDetector(
            onTap: () { /* Write listener code here */ },
            child: Icon(
              Icons.menu,
              color: Colors.black,
              // add custom icons also
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            title: new Text('Messages'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile')
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: new Container(
            child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(), //
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: tripsList.length,
                itemBuilder: (BuildContext context, int index) =>
                    buildTripCard(context, index)),
        ),
        //backgroundColor: AppColors.backgroundColor,

      ),
    );
  }
}

buildTripCard(BuildContext context, int index) {
   return new Container(
      child: Padding(
           padding: EdgeInsets.only(left: 8,right: 8,top: 8),
           child: Card(
             child: Column(
               children: [
                 Row(
                   children: [
                     Container(
                       height: 78,
                       width: 68,
                       child: Image.asset('assets/images/doc.jpg'),
                     ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Text(
                             'Gary Lance',
                             style: TextStyle(
                               fontSize: 16,
                               color: Color(0xff262628),
                               fontFamily: 'DMSans-Bold',
                             ),
                           ),
                           Text(
                             'Genreal Physicyts',
                             style: TextStyle(
                               fontSize: 16,
                               color: Color(0xffCBC9D9),
                               fontFamily: 'DMSans-Bold',
                             ),
                           ),
                         ],
                       ),
                     ),
                   ],
                 ),
                 Divider(),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Row(
                     children: <Widget>[
                       Text(
                         'Call Now',
                         style: TextStyle(
                           fontSize: 14,
                           color: Color(0xff2DCED6),
                           fontFamily: 'DMSans-Bold',
                         ),
                       ),
                       SizedBox(
                         width: 3,
                       ),
                       Container(
                         margin: EdgeInsets.only(left: 10),
                         child: Text(
                           'Book Appoinment',
                           style: TextStyle(
                             fontSize: 14,
                             color: Color(0xff2DCED6),
                             fontFamily: 'DMSans-Bold',
                           ),
                         ),
                       ),
                       SizedBox(
                         width: 3,
                       ),
                       Container(
                           height: 4,
                           width: 4,
                           child: Image.asset('assets/images/dot.png')
                       ),
                       SizedBox(
                         width: 5,
                       ),
                       Container(
                           margin: EdgeInsets.only(left: 70),
                           height: 15,
                           width: 15,
                           child: Image.asset('assets/images/phone.png')
                       ),

                     ],
                   ),
                 )
               ],
             ),
           ),
        ),
   );
}

